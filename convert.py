import argparse
import glob
import multiprocessing as mp
import os
import re
import warnings
from functools import partial
from timeit import default_timer
import numpy as np
import pandas as pd

# python3 please, and relies on numpy/pandas
# TODO: check 552-07, day 3 -- I *think* the aff/unaff labels were switched
# [site == '552' & subject == '07' & day == '003', task := ifelse(task == 'aff', 'unaff', 'aff')]


def write_meas(directory):
    with open(os.path.join(directory, 'Meas.txt'), 'w+') as f:
        f.write('1.0,'*8 + '1.0\n')
        f.write('The above parameters are:\n')
        f.write('Mark1toelbx, Mark1toelbz, Mark1toelby, Mark1tofingx, Mark1tofingz, ' +
                'Mark1tofingy, Mark2toshx, Mark2toshz, Mark2toshy\n')


def write_targ(directory, tgt):
    with open(os.path.join(directory, 'Targ.txt'), 'w+') as f:
        f.write('TarNo,Startx,Starty,Endx,Endy,Direction,Length\n')
        # write other data here from target
        for i in tgt:
            f.write(str(i['TarNo']) + ',' + str(i['Startx']) + ',' + str(i['Starty']) + ',')
            f.write(str(i['Endx']) + ',' + str(i['Endy']) + ',' + str(i['Direction']) + ',')
            f.write(str(i['Length']) + '\n')


def write_sess(directory, tgt):
    """
    Equivalent (sort of) to the tgt files
    """
    with open(os.path.join(directory, 'Sess.txt'), 'w+') as f:
        f.write('Trial,Duration,CircleTime,TargetN,ShowRevSq,Rot,DisplaceX,DisplaceY,' +
                'FBstr,KRstr,delayEntered,condTagNum,Gain,TargShiftAngle,Freeze,CondTrial')
        # for index, row in tgt.iterrows():
        for index, i in enumerate(tgt):
            f.write(str(index) + ',')  # index
            f.write(str(3) + ',')  # duration (from config.h)
            f.write(str(0.8) + ',')  # CircleTime (from config.h)
            f.write(str(i['TarNo']) + ',')  # TargetN (should derive from tFile.tgt)
            f.write(str(0) + ',')  # ShowRevSq (unused)
            f.write(str(0) + ',')  # Rot (unused?)
            f.write(str(0) + ',')  # DisplaceX (unused?)
            f.write(str(0) + ',')  # DisplaceY (unused?)
            f.write(str(1) + ',')  # online feedback (always yes?)
            f.write(str(1) + ',')  # knowledge of results (always yes?)
            f.write(str(0) + ',')  # delay for feedback
            f.write(str(0) + ',')  # label for condition (unused?)
            f.write(str(1) + ',')  # gain (guessing 1 is identity)
            f.write(str(0) + ',')  # target jump angle (no jumps)
            f.write(str(0) + ',')  # freeze (unused)
            f.write(str(index) + '\n')  # trial number within condition (???)


def write_cal(directory):
    """
    The few physical/screen dims are lifted from various `config.h` files. Those files also
    contain offset info, but I'm holding off on adding until I know we'll use them for sure
    (offsets change amongst the offsets)
    """
    with open(os.path.join(directory, 'Cal.txt'), 'w+') as f:
        f.write('1.21,0.680625,1920,1080,1.21,0.680625,1,1,0,0,0,0.09,0.067,0,0,0,0\n')
        f.write('The above parameters are:\n')
        f.write('Screenx, ScreenY, Xpixels, Ypixels, Xmeters, Ymeters, Xmeteroffset, ' +
                'Ymeteroffset, rotaz, rotel, rotrl, wkspWidthMeters, wkspHeightMeters, ' +
                'wkspWidthReflPix, wkspHeightReflPix, wkspOriXreflPix, wkspOriYreflPix\n')
        # TODO: not sure about these, the value in config.h is 0.5995
        f.write('0	offXfld.text\n')
        f.write('0	offYfld.text\n')
        f.write('0	offZfld.text\n')
        f.write('1920	screenXpixFld.text\n')
        f.write('1080	screenYpixFld.text\n')
        # TODO: we don't have a workspace measurement?
        f.write('0	fourMarkerWnd.WkspWidthMetersFld.text\n')
        f.write('0	fourMarkerWnd.WkspHeightMetersFld.text\n')


def write_glob(directory, tgt):
    with open(os.path.join(directory, 'Glob.txt'), 'w+') as f:
        f.write(
            'Global parameters for run: values of several user-set parameters in CtrlWind\n')
        f.write('1\tAcqHardwPop.listIndex\n')
        f.write('Bird\tAcqHardPop.text\n')
        f.write('1\trecordFreqPop.listIndex\n')
        f.write('130\trecordFreqPop.text\n')
        f.write('0\tMouseBox.value\n')
        f.write('0\tRhytBox.value\n')
        f.write('1.5\tRhytPerFld.text\n')
        f.write('0.8\tITIfld.text\n')
        f.write('0\tIBIfld.text\n')
        f.write(str(tgt) + '\tBlockMovesFld.text\n')
        f.write('0\tshowLetBox.value\n')
        f.write('0\tshowTargBox.value\n')
        f.write('0\ttargShiftBox.value\n')
        f.write('0\ttargShiftLatFld.text\n')
        f.write('0\tTshiftLandMPop.listIndex\n')
        f.write('MoveStart\tTshiftLandMPop.text\n')
        f.write('0\tTMSbox.value\n')
        f.write('0\tTMSLandMarkPop.listIndex\n')
        f.write('TrialStart\tTMSLandMarkPop.text\n')
        f.write('0\trTMSbox.value\n')
        f.write('1.0\trTMSperFld.text\n')
        f.write('15\trTMSdurFld.text\n')
        f.write('1\tScoreFBBox.value\n')
        f.write('0\ttime2FBFld.text\n')
        f.write('0\tInTargFBBox.value\n')
        f.write('.000\tsteadyTimeFld.text\n')
        f.write('0\tVmaxFBbox.value\n')
        f.write('0\tVcirTarFBbox.value\n')
        f.write('\tParameter no longer used\n')
        f.write('\tParameter no longer used\n')
        f.write('20\tvMinSetFld.text\n')
        f.write('40\tvMaxSetFld.text\n')
        f.write('120\tvLimFld.text\n')
        f.write('0\tvMaxReqBox.value\n')
        f.write('0\ttargReqBox.value\n')
        f.write('2\tkrStepFld.text\n')
        f.write('1\ttimeKRFld.text\n')
        f.write('0.8\tpreTriggerFld.text\n')
        # this (and the following two fields) are derived from config.h
        f.write('0.4\tFBRadiusFld.text\n')
        f.write('1.5\ttarRadiusFld.text\n')
        f.write('1.0\tstartRadiusFld.text\n')
        f.write('N\tmassFld.text\n')
        f.write('4\tNRecErrFld.text\n')
        f.write('0\tgreyOnHitBttn.value\n')
        f.write('0\tsoundWhenHitBox.value\n')
        f.write('1024\tSubjWxOffsetFld.text\n')
        f.write('0\tshowTargBefBox.value\n')
        f.write('0\tcollectBox.value\n')
        f.write('1\tnoRotITIbox.value\n')
        f.write('1\tflipScrLRbox.value\n')
        f.write('0\tcolorFBbox.value\n')
        f.write('1\tptToPtbox.value\n')
        f.write('0\tredOnNoFBbox.value\n')
        f.write('0\tdoEyeRecBox.value\n')
        f.write('0\tRewriteBox.value\n')
        f.write('0\tportAssignWind.Bird1PortFld.text\n')
        f.write('1\tportAssignWind.Bird2PortFld.text\n')
        f.write('0\tportAssignWind.ReflPortFld.text\n')
        f.write('0\tportAssignWind.TTLPortFld.text\n')
        f.write('N003\trunInfoDlog.subjInitFld.text\n')
        f.write('\trunInfoDlog.subjNameFld.text\n')
        f.write('\trunInfoDlog.displDistFld.text\n')
        f.write('\trunInfoDlog.DisplWidthFld.text\n')
        f.write('\trunInfoDlog.DisplHtFld.text\n')
        f.write('\trunInfoDlog.refDistanceFld.text\n')
        f.write('SPUR4.2s\trunInfoDlog.AcqSoftwFld.text\n')
        f.write('***********\n')
        f.write('NotesFld.text appears below:\n')
        f.write('\n'*7)
        f.write("""EndOfNotes
0\toutSegmKRbox.value
0\tshowReversalBox.value
1\thideAtRevBox.value
0\tFBstartTimeFld.text
1000\tFBendTimeFld.text
1\tshowLastFBptBox.value
1\toverrideFBbox.value
1\tbullsEyeAreabox.value
.3\tcirTimJitFld.text
0\tplaySndBox.value
\tCountdnSecFld.text
0\tFixedPitchBox.value
0\tcircleLeaveReqBox.value
1\tRevInTargFBbox.value
0\tITIjitterFld.text
0\tstartByMRbox.value
0\tcursOffBox.value
0\tvoiceVmaxMissBox.value
0\tskillErrBox.value
0\tvoiceVmaxValBox.value
1\trunInfoDlog.RHbttn.value
0\trunInfoDlog.LHbttn.value
0\tFixPtOnBox.value
\tFixPtXFld.text
\tFixPtYFld.text
\tFixPtRadiusFld.text
1000000\tTarLifeBaseFld.text
\tTarLifeSlopeFld.text
0\tflipLRBox.value
0\tflipUDBox.value
0\trepeatFalseBox.value
0\tdrawChannelBox.value
\tchannelTypeFld.text
\tchScoreScaleFld.text
\tchScoreFactFld.text
1\tEndTrialSoonBox.value
0.5\tPostTrlLingFld.text
0\tPaceInTrialBox.value
\tPaceInTrlBPMFld.text
\tGoCueFld.text
0\tSmallCursBox.value
1\trevContingBox.value
0\tgiveChFBbox.value
EOF\n""")


def sanitize_path(pth):
    # expand tilde & get the absolute path
    return os.path.realpath(os.path.expanduser(pth))


def find_dirs_with_file(fn, search_path):
    """Find (nested) directories containing a file with a particular name, `fn`."""
    filename = fn.lower()
    tfiles = list()
    for dirpath, dirnames, filenames in os.walk(search_path):
        filenames = [f.lower() for f in filenames]
        if filename in filenames:
            tfiles.append(dirpath)
    return tfiles


def file_len(fname):
    """Calculate file length"""
    i = 0
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1


def split_path(path):
    """Split path into component parts"""
    folders = []
    while 1:
        path, folder = os.path.split(path)
        if folder != "":
            folders.append(folder)
        else:
            if path != "":
                folders.append(path)
            break
    folders.reverse()
    return folders


def find_good_subdirs(inpath, wq):
    # get all directories with tfile
    tfile_dirs = find_dirs_with_file('tFile.tgt', inpath)
    # nix "Meas" dirs (not used AFAIK)
    tfile_dirs = [tf for tf in tfile_dirs if 'meas' not in tf.lower()]
    # check tFile health--should be at least some length,
    # and/or have some data
    tfile_lengths = [file_len(tf) for tf in [os.path.join(
        xx, 'tFile.tgt') for xx in tfile_dirs]]
    tfile_bad_length = [idx for idx, x in enumerate(tfile_lengths) if x < 2]
    for idx in sorted(tfile_bad_length, reverse=True):
        wq.put('tFile at %s is of improper length, so ignoring.' %
               tfile_dirs[idx])
        del tfile_dirs[idx]
    return tfile_dirs


def check_duplicate_trialnames(path):
    datfiles = glob.glob(os.path.join(path, '*.dat'))  # list all dat in dir
    datfiles = [os.path.basename(i) for i in datfiles]
    # get the three numbers between two underscores
    datfiles = [re.search('_(.*)_', s).group(1)
                for s in datfiles if 'trial' in s.lower()]
    return len(datfiles) != len(set(datfiles))


def distance(x1, y1, x2, y2):
    return np.sqrt(np.square(x2 - x1) + np.square(y2 - y1))


def dir_to_igor(x, in_path, out_path, labels, wq):
    """Operates per-directory.
    Check for improper timestamps/lengths,
    missing "actual" movement.
    """
    out_name = None
    # should be ~80 for a whole block (but could be less if early stop)
    datfiles = glob.glob(os.path.join(x, 'Trial*.dat'))
    # do a first pass to figure out handedness details
    split_folds = split_path(datfiles[0])
    subj_id, day, exp_type = split_folds[-2].split('_')
    # make a more "standard" subject name (can't count on all zeros or dashes being there)
    # not super robust, e.g. 550-10 and 550-01 (if they existed) would match
    subj_id = re.sub('[^1-9]', '', subj_id)  # 550-02 -> 552, 551-17 -> 55117
    # get the ground-truth paretic side for this subject
    ne_subj = labels['mod_id'] == subj_id
    if any(ne_subj):
        tmp = labels[ne_subj]['side']
        paretic_side = tmp.iloc[0]
    else:
        wq.put('subject at %s not in labels.' % x)
        paretic_side = 'R'
    non_paretic_side = [x for x in ['L', 'R'] if paretic_side != x][0]  # roundabout way to get the other letter
    # see what the active hand *should* be (given the folder name)
    if 'u' in exp_type.lower():
        expected_active_hand = non_paretic_side
    else:
        expected_active_hand = paretic_side
    # now we need to check the truth here
    # we'll read all the data in now, and do our sanity checks
    datlist = []
    right_hand = 0
    for i in datfiles:
        try:
            keyvals, tbl = read_trial(i)
        except:
            wq.put('Malformed data in %s' % i)
            continue
        # check that target was shown
        # usually only happens when the block finished prematurely
        if not any(tbl['TargetX'] > -90):
            wq.put('Target never shown in %s' % i)
            continue
        # check timestamps
        time_col_name = [rr for rr in tbl.columns if 'time' in rr.lower()][0]
        time_col = tbl[time_col_name][::4]
        diffs = np.diff(time_col)
        if sum(diffs < 1e-6) > 10:
            # three sources (AFAIK):
            # 1. Overflow (e.g. 550-03_090_A)
            # 2. Weird thing (e.g. 551-2_000_A\Trial_000)
            # 3. No resolution (e.g. 552-03_003_A)
            if sum(time_col < 0) > 50:
                wq.put('Probable time overflow in %s, but trying to write anyway.' % i)
            else:
                wq.put('Timestamp either too coarse or unxpected resolution in %s' % i)
                continue
        # check if all sensors connected
        for rr in range(4):
            tmp = tbl[tbl['Device_Num'] == rr+1]['HandX'] == 0.0
            if sum(tmp) > 20:
                wq.put('Sensor %i in %s might be weird?' % (rr+1, i))
                continue
        # add to list for later
        datlist.append({'filename': i, 'keyvals': keyvals, 'tbl': tbl})
        # now we vote for the active hand
        left_hand_mv = tbl[tbl['Device_Num'] == 1]
        left_var = (np.var(left_hand_mv['HandX']) +
                    np.var(left_hand_mv['HandY']))/2
        right_hand_mv = tbl[tbl['Device_Num'] == 3]
        right_var = (np.var(right_hand_mv['HandX']) +
                     np.var(right_hand_mv['HandY']))/2
        if right_var > left_var:
            right_hand += 1
        else:
            right_hand -= 1

    sess_list = []
    targ_list = []
    # print('Right hand final count: %d' % right_hand)
    for r in datlist:
        # assign to match old thing
        i = r['filename']
        keyvals = r['keyvals']
        tbl = r['tbl']
        # cross fingers that everyone used underscores
        info = {}  # things that go into header of data file
        split_folds = split_path(i)
        info['trial_number'] = split_folds[-1].split('_')[1]
        info['subj_id'], info['day'], info['exp_type'] = split_folds[-2].split('_')
        info.update(keyvals)
        # now fill out the files
        # chop off the time before the target is visible (never used, AFAIK)
        idx = tbl[tbl['TargetX'].gt(-90)].index[0]
        tbl = tbl[idx:].copy()
        target_x, target_y = tbl[['TargetX', 'TargetY']].iloc[0]
        start_x, start_y = tbl[['StartX', 'StartY']].iloc[0]
        info['angle'] = np.arctan2(
            target_y - start_y, target_x - start_x) * 180 / np.pi
        info.update({'target_x': target_x, 'target_y': target_y,
                     'start_x': start_x, 'start_y': start_y})
        # correct timestamp?
        # only if int-based, but if float don't try (probably already secs, e.g. later UZ data)
        if tbl[time_col_name].dtype == 'int64':
            tbl[time_col_name] /= 1000.0
        # we need to go from long to wide
        if sum(tbl[time_col_name] < 0) > 20:
            # feed in a fake, but suitable timestamp
            nr = int(tbl.shape[0]/4)
            out_tab = pd.DataFrame(data={time_col_name: np.arange(0, nr/130, 1/130)})
        else:
            out_tab = pd.DataFrame(data=tbl[time_col_name][::4])
        # first, fill in columns we won't use (we had four sensors: two per hand, and
        # two per proximal arm (so nothing corresponding strictly to elbow, shoulder))
        unused_cols = ['X1', 'Y1', 'Z1', 'X2', 'Y2', 'Z2', 'AZ1', 'EL1', 'RL1', 'AZ2',
                       'EL2', 'RL2', 'shx', 'shy', 'shz', 'elbx', 'elby', 'elbz']
        out_tab = out_tab.assign(**dict.fromkeys(unused_cols, 0.0))
        # now, use a heuristic to decide the "active" hand
        # previously, all files "voted" for the choice based off
        # which hand had greater variance
        if right_hand > 0:
            hand_dev = 3
            sh_dev = 4
            nm = 'R'
        else:
            hand_dev = 1
            sh_dev = 2
            nm = 'L'
        if nm != expected_active_hand:
            wq.put(
                """Trial %s aff/unaff folder naming may be wrong
                (empirical: %s, folder: %s). Assuming empirical version is correct.
                """ % (i, nm, expected_active_hand))
        out_tab['fingx'] = tbl[(tbl['Device_Num'] == hand_dev)]['HandX'].values
        out_tab['fingy'] = tbl[(tbl['Device_Num'] == hand_dev)]['HandY'].values
        out_tab['fingz'] = tbl[(tbl['Device_Num'] == hand_dev)]['HandZ'].values
        out_tab['shx'] = tbl[(tbl['Device_Num'] == sh_dev)]['HandX'].values
        out_tab['shy'] = tbl[(tbl['Device_Num'] == sh_dev)]['HandY'].values
        out_tab['shz'] = tbl[(tbl['Device_Num'] == sh_dev)]['HandZ'].values
        out_tab['X1'] = tbl[(tbl['Device_Num'] == 1)]['HandX'].values
        out_tab['Y1'] = tbl[(tbl['Device_Num'] == 1)]['HandY'].values
        out_tab['Z1'] = tbl[(tbl['Device_Num'] == 1)]['HandZ'].values
        out_tab['X2'] = tbl[(tbl['Device_Num'] == 3)]['HandX'].values
        out_tab['Y2'] = tbl[(tbl['Device_Num'] == 3)]['HandY'].values
        out_tab['Z2'] = tbl[(tbl['Device_Num'] == 3)]['HandZ'].values

        out_tab = out_tab.rename(index=str, columns={time_col_name: 'sec'})
        info['guessed_hand'] = nm  # save the guess, for sanity check

        # target number
        x = info['target_x'] - info['start_x']
        y = info['target_y'] - info['start_y']
        if (np.sign([x, y]) == [1, 1]).all():
            target_num = 0
        elif (np.sign([x, y]) == [-1, 1]).all():
            target_num = 1
        elif (np.sign([x, y]) == [-1, -1]).all():
            target_num = 2
        elif (np.sign([x, y]) == [1, -1]).all():
            target_num = 3
        else:
            target_num = -1
        dist = distance(target_x, target_y, start_x, start_y)
        # write header (from info)
        out_name = out_path + i[len(in_path):]
        # check if directories exist
        tmpdir = os.path.dirname(out_name)
        try:
            os.makedirs(tmpdir)
        except OSError:
            # if directory is already created, don't make it
            pass
        # clobber file if it exists (risky, I suppose?)
        # also change extension from .dat to .txt
        # pathlib.Path(out_path).with_suffix('.txt') might be better?
        out_name = os.path.splitext(out_name)[0]
        out_name = out_name + '.txt'
        with open(out_name, 'w+') as f:
            f.write('HEADER:\n')
            f.write('Filename\t' + os.path.basename(out_name) + '\n')
            f.write('Conditions\t' + info['exp_type'] + '\n')
            f.write('Date\t' + info['Date'] + '\n')
            f.write('Time\t' + info['Time'] + '\n')
            f.write('Columns\t' + str(out_tab.shape[1]) + '\n')
            f.write('Lines\t' + str(out_tab.shape[0]) + '\n')
            f.write('StartY\t' + str(info['start_y']) + '\n')
            f.write('StartX\t' + str(info['start_x']) + '\n')
            f.write('EndX\t' + str(info['target_x']) + '\n')
            f.write('EndY\t' + str(info['target_y']) + '\n')
            f.write('Day\t' + info['day'] + '\n')
            f.write('SubjName\t' + info['subj_id'] + '\n')
            f.write('sampleBin\t' + str(1/(float(info['Sampling_Rate']))) + '\n')
            f.write('AcqSoftw\t' + info['Tracker'] + 'KinereachCpp\n')
            f.write('StartRadius\t0.01\n')  # in meters
            f.write('popTarRadius\t0.015\n')  # in meters
            f.write('TarNo\t' + str(target_num) + '\n')
            f.write('direction\t' + str(info['angle']) + '\n')
            f.write('length\t' + str(dist) + '\n')
            f.write('guessed_hand\t' + info['guessed_hand'] + '\n')
            f.write('******************\n')
            f.write('Names:\t\n')
            f.write('\t'.join(out_tab.columns) + '\n')  # header for data
            f.write('DATA:\t\n')
            out_tab.to_csv(f, sep='\t', header=False, index=False)
        # write settings for sess, targ
        sess_list.append({'TarNo': target_num, 'Startx': info['start_x'],
                          'Starty': info['start_y'], 'Endx': info['target_x'],
                          'Endy': info['target_y'], 'Direction': info['angle'],
                          'Length': dist})
        targ_list.append({'TarNo': target_num})
    # do the other settings files (maybe not required?)
    # if we had at least one data file, we'll make the supplementary settings files
    # TODO: currently not anything useful, other than to make Igor happy.
    # The data here would be redundant with that in the header of the data files
    if out_name:
        op = os.path.dirname(out_name)
        write_cal(op)
        write_glob(op, len(sess_list))
        write_meas(op)
        write_sess(op, targ_list)
        write_targ(op, sess_list)


def read_trial(filename):
    with open(filename, 'r') as f:
        header_keyvals = {}
        i = 0
        # parse header first
        for line in f:
            if line == '--\n':
                break
            name, var = line.partition(' ')[::2]
            name = name.replace(':', '')
            var = var.replace('\n', '')
            header_keyvals[name] = var
            i += 1
        for line in f:
            line = line.replace(' \n', '')
            header = line.split(' ')
            break
        i += 3
    # read the bulk of the data
    tbl = pd.read_table(filename, sep=' ', header=None, skiprows=i)
    tbl.columns = header
    return header_keyvals, tbl


def convert_to_igor(in_path, out_path, labels):
    """in_path: top-level dir for data
       out_path: top-level dir where new data will land
       labels: two-column pandas dataframe with columns ('id', 'paretic_side')
    """
    # find tFile.tgts
    m = mp.Manager()
    warn_queue = m.Queue()
    good_subdirs = find_good_subdirs(in_path, warn_queue)
    # good_subdirs contains all directories with a tFile.tgt file.
    # next, check for (partial) duplicates of filenames
    # e.g. if there's two Trial_003_xxx
    # because less trivial to tell which set is the "correct" one
    for i in good_subdirs:
        if check_duplicate_trialnames(i):
            warn_queue.put('Duplicated trial names in %s, so ignoring.' % i)
            good_subdirs.remove(i)

    # now we'll start converting files. There are two more situations
    # where we'll skip the file and/or directory:
    #   1. If there was no data before the "start" of the trial (experiment quit pre-stimulus)
    #   2. Bad timestamps (just in UZ data, AFAIK).
    # This will be parallelized, as well. We'll do per-block parallelization
    # (vs per-trial parallelization), so we don't have to keep spinning up new
    # python instances
    with mp.Pool(mp.cpu_count() - 1) as p:
        proc_file = partial(dir_to_igor, in_path=in_path, out_path=out_path, labels=labels, wq=warn_queue)
        p.map(proc_file, (i for i in good_subdirs))
    logname = os.path.join(out_path, 'log.txt')
    # delete the log file if it already exists
    if os.path.isfile(logname):
        os.remove(logname)
    while True:
        try:
            msg = warn_queue.get_nowait()
            with open(os.path.join(out_path, 'log.txt'), 'a') as f:
                f.write(msg + '\n')
        except mp.queues.Empty:
            break


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Turn SMARTS2 files in Igor-compatible ones.',
                                     epilog='Example: `python convert.py ~/extra/A_SMARTS2SourceFiles/good /home/exp/extra/A_SMARTS2SourceFiles/SMARTS2_labels.xlsx`')
    parser.add_argument('in_path', type=str, help='Path to the source files.')
    parser.add_argument('-o', '--out_path', type=str, default=None,
                        help='Optional output path. Defaults to in_path + "_igor"')
    parser.add_argument('label_file', type=str,
                        help='Path to the label file (column 1 is ID, column 2 is paretic side).')
    args = parser.parse_args()
    in_path = sanitize_path(args.in_path)
    if args.out_path:
        out_path = sanitize_path(args.out_path)
        if os.path.samefile(in_path, out_path):
            raise ValueError('In and out paths should not match.')
    else:
        out_path = in_path + '_igor'
    label_path = sanitize_path(args.label_file)
    t0 = default_timer()
    labels = pd.read_excel(label_path)
    labels.columns = ['id', 'side']  # original columns have spaces
    labels['mod_id'] = [re.sub('[^1-9]', '', x) for x in labels['id']]  # make a special column for comparison
    convert_to_igor(in_path, out_path, labels)
    print('Total duration: %s' % (default_timer() - t0))
