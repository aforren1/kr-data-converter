
## Installation

1. Install python 3 (at least 3.5).
2. Using that python, install dependencies (`pip install numpy pandas`)

## Usage
The program takes two required arguments and one optional one:

python convert.py [INPUT PATH] [LABEL FILE] -o [OUTPUT PATH]

Where all paths/files can be absolute or relative paths. When the output path is unspecified, it'll default to the input path with "_igor" appended.

The label file has two columns, and should look something like:

```
id       paretic_side
550-01       L
552-32       R
...
```

Example usage:

```
python convert.py ~/extra/A_SMARTS2SourceFiles/good ~/extra/A_SMARTS2SourceFiles/SMARTS2_labels.xlsx
```

Help is available using

```
python convert.py -h
```