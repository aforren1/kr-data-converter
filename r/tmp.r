
find_peak_speed <- function(speed, accel) {
  # find the first peak speed (where speed > 10cm/s and accel switches signs)
  peak_speed_index <- NA
  for (i in 1:length(speed)) {
    if (i < 20) { next } # ignore the first 500ms, there may be artifacts from smooth
    if (speed[i] > 0.08) {
      if (sign(accel[i]) != sign(accel[i-1])) {
        peak_speed_index <- i
        break
      }
    }
  }
  peak_speed_index
}

angle_diff <- function(th1, th2) {
    180 - abs(180 - abs(th1 - th2) %% 360)
}

label_movement <- function(speed, accel, angle_from_origin,
                          angle_of_target, perc_of_dist)
{
    # find the trial-by-trial movement params
    # return the following:
    # 1. a logical vector, for subsetting the movement (all FALSE if no peak speed or ending)
    # 2. a logical vector, whether the peak speed was exceeded or not
    # 3. a logical vector, whether an ending was found for the movement
    # 4. a logical vector, whether the direction of the movement at peak speed
    #    (if exists) was >= 90deg from target
    # 5. a logical vector, whether the movement ended <= 30% of the target distance
    peak_reached <- TRUE
    peak_speed_index <- find_peak_speed(speed, accel)
    if (is.na(peak_speed_index)) {
        peak_reached <- FALSE
    }

    # 
    if (peak_reached) {
        # find the nearest index (in the past) where the speed exceeded 2cm/s
        # == start of the movement
        beginning_index <- NA
        pre_speed <- rev(speed[1:peak_speed_index])
        for (i in 1:length(pre_speed)) {
            # find the first time the speed drops below/equal to 2cm/s, then get the prev index
            if (!is.na(pre_speed[i]) && pre_speed[i] <= 0.02) {
                beginning_index <- i - 1
                break
            }
        }
        if (is.na(beginning_index)) { # if the speed never dropped below 2cm/s
            message('beginning not found.')
            beginning_index <- 1
        } else {
            beginning_index <- peak_speed_index - beginning_index + 1
        }
        # find the nearest index (in the future) where the speed drops below 2cm/s for 0.1s
        post_index <- NA
        counter <- 0
        for (i in 1:length(speed)) {
            if (i <= peak_speed_index) { next }
            if (speed[i] <= 0.02) {
                counter <- counter + 1
            } else {
                counter <- 0
            }
            if (counter >= 20) { # probably either 13 or 26 (corresponding to 100 or 200 ms)
                post_index <- i
                break
            }
        }
        if (is.na(post_index)) {
            post_index <- length(speed)
        }
        movement_subset <- rep(FALSE, length(speed))
        movement_subset[beginning_index:post_index] <- TRUE
        peak_vec <- rep(TRUE, length(speed))
        if (angle_diff(angle_from_origin[peak_speed_index], angle_of_target[peak_speed_index]) >= 90) {
            direc_vec <- rep(FALSE, length(speed))
        } else {
            direc_vec <- rep(TRUE, length(speed))
        }
        if (tail(perc_of_dist[movement_subset], 1) <= 0.3) {
            dist_vec <- rep(FALSE, length(speed))
        } else {
            dist_vec <- rep(TRUE, length(speed))
        }

    } else { # peak not reached, we won't calc these things for now
        movement_subset <- rep(FALSE, length(speed))
        peak_vec <- rep(FALSE, length(speed))
        direc_vec <- rep(FALSE, length(speed))
        dist_vec <- rep(FALSE, length(speed))
    }
    list(movement_subset, peak_vec, direc_vec, dist_vec)
}

dist_from_target <- function(x, y, tx, ty)
{
  sqrt((x - tx)^2 + (y - ty)^2)
}

label_turn_point <- function(px, py, speed, accel, ref_angle, target_x, target_y, peak_exceeded)
{
  len <- length(px)
  # find the peak speed again
  if (!peak_exceeded[1]) {
    return(rep(FALSE, len))
  }
  ps_index <- find_peak_speed(speed, accel)
  out <- rep(TRUE, len)
  if (len-ps_index < 10) { return(out)}
  # run DouglasPeucker to simplify trajectory (only on points after peak speed)
  dp_pairs <- kmlShape::DouglasPeuckerEpsilon(px[ps_index:len], py[ps_index:len], 0.001)
  res <- NA
  if (nrow(dp_pairs) < 2) { return(out) } # impossible?
  for (i in 2:nrow(dp_pairs)) {
    abs_angle <- atan2(dp_pairs$y[i] - dp_pairs$y[i-1],
                       dp_pairs$x[i] - dp_pairs$x[i-1]) * 180/pi
    if (angle_diff(abs_angle, ref_angle) >= 120) { #& 
        #dist_from_target(dp_pairs$x[i], dp_pairs$y[i], target_x, target_y) > dist_from_target(dp_pairs$x[i-1], dp_pairs$y[i-1], target_x, target_y)) {
      # match nearest point on trajectory
      dists <- rep(0, length(px))
      for (j in 1:length(px)) {
        if (j < ps_index) {
          dists[j] <- 10000
        }
        dists[j] <- sqrt((dp_pairs$x[i] - px[j])^2 + (dp_pairs$y[i] - py[j]) ^ 2)
      }
      res <- which(dists==min(dists))
      break
    }
  }
  if(!is.na(res)) {
    out[res:len] <- FALSE
    return(out)
  }
  return(out)
}

dist_from <- function(x0, x1, y0, y1)
{
  sqrt((x1 - x0)^2 + (y1 - y0)^2)
}
at_least_half <- function(sm_x, sm_y, tx, ty) {
  # if displacement is smaller than half the distance, we'll ignore for now
  tl <- length(sm_x)
  tg_dist <- dist_from(tx, 0, ty, 0)
  len <- dist_from(sm_x[1], sm_x[tl], sm_y[1], sm_y[tl])
  if (len/tg_dist < 0.5)
  {
    val <- FALSE
  } else {
    val <- TRUE
  }
  rep(val, tl)
}

close_to_start <- function(sm_x, sm_y, tx, ty) {
  # see if the start of the trajectory is more than a third of the total distance from origin
  d0 <- dist_from(sm_x[1], 0, sm_y[1], 0)
  d1 <- dist_from(tx, 0, ty, 0)
  if (d0/d1 > (1/3)) {
    val <- FALSE
  } else {
    val <- TRUE
  }
  rep(val, length(sm_x))
}