library(data.table)
library(stringr)
library(snakecase)

# from https://stackoverflow.com/questions/29214932/split-a-file-path-into-folder-names-vector
split_path <- function(path, mustWork = FALSE, rev = TRUE) {
  output <- c(strsplit(dirname(normalizePath(path,mustWork = mustWork)),
                       "/|\\\\")[[1]], basename(path))
  ifelse(rev, return(rev(output)), return(output))
}

is_error <- function(x) inherits(x, 'try-error')
# find all subdirectories w/ a tfile
find_tfiles <- function(x) { length(list.files(x, pattern = '.tgt')) == 1 }
# find the point-to-point blocks
find_ptp <- function(x) { !grepl('meas', tolower(basename(x))) }

import_reach_data <- function(base_dir) {
    dirs <- list.dirs(base_dir, full.names = TRUE, recursive = TRUE)
    sub_dirs <- dirs[sapply(dirs, find_tfiles)] # only list directories that have a tfile.tgt
    sub_dirs <- sub_dirs[sapply(sub_dirs, find_ptp)] # then only directories with the 'meas' file

    bigger_list <- list()
    for (i in 1:length(sub_dirs)) {
        all_data_files <- list.files(sub_dirs[i], pattern='.dat', full.names=TRUE)
        all_data_files <- all_data_files[grepl('Trial', all_data_files)]
        # determine whether there's something wrong with the directory
        if (length(all_data_files) < 2) {
            message(paste('Short block, skipping:', sub_dirs[i])); next
        }
        numbs <- sapply(basename(all_data_files), substr, 7, 9, USE.NAMES = FALSE)
        if (anyDuplicated(numbs)) { message(paste('Duplicated trial numbers, something wrong with:', sub_dirs[i])); next }
        # check if tfile is worthy (skip if weird, who knows where the data came from)
        # we don't need to actually use the tfile for anything
        tfile <- try(fread(file.path(sub_dirs[i], 'tFile.tgt'), header = FALSE))
        if (is_error(tfile)) { message(paste('empty tfile:', sub_dirs[i])); next }
        if (nrow(tfile) < 2) { message(paste('short tfile:', sub_dirs[i])); next }
        path_stuff <- split_path(sub_dirs[i])
        message(path_stuff[1])
        tmp <- gsub('[0-9]', '', x = path_stuff[1])
        type_of_task <- gsub('[[:punct:]]', '', x = tmp)
        site <- substr(path_stuff[2], 1, 3) # first three numbers are the site

        # next 1 to 3 numbers are the subject
        # strong assumption that underscores aren't used early...
        first_us <- str_locate(path_stuff[2], '_')[1] - 1
        first_chunk <- substr(path_stuff[2], 4, first_us)
        subject_id <- gsub('[^0-9]', '', first_chunk)
        day <- strsplit(path_stuff[2], '_')[[1]][2]

        data_list <- list()
        for (j in 1:length(all_data_files)) {
            # read trial-by-trial
            # check if the data is worthy
            # sanity check lengths
            header <- try(fread(all_data_files[j], skip = 7, nrows = 0, drop = c(11, 12)))
            if (is_error(header)) { message(paste('empty header:', all_data_files[j])); next }
            # These were the couple of sessions at UZ with 
            # a bad timestamp + inconsistent sampling
            if ('fake_time' %in% names(header) && site=='552') { next }
            # other checks on data integrity
            dat <- try(fread(all_data_files[j], skip = 9, header = FALSE, drop = c(11, 12)))
            if (is_error(dat)) { message(paste('empty data:', all_data_files[j])); next }
            if (nrow(dat) < 10) { message(paste('short data:', all_data_files[j])); next }
            names(dat) <- to_snake_case(names(header))
            if (!any(dat$target_x > -90)) { 
                message(paste('no in-trial data:', all_data_files[j])); next
            }
            dat <- dat[min(which(target_x > -90)):.N] # remove pre-trial mess
            dat[, `:=`(px = hand_x - start_x,
                       py = hand_y - start_y,
                       target_x = target_x - start_x,
                       target_y = target_y - start_y)]
            dat[, c('start_x', 'start_y', 'velocity',
                    'hand_x', 'hand_y', 'hand_z') := NULL] # velocity was calc'd online
            dat <- dat[device_num == 1 | device_num == 3]
            dat[, hand_sensor := ifelse(device_num == 1, 'left', 'right')]
            dat[, device_num := NULL]
            
            dat[, `:=`(subject = subject_id,
                        day = day,
                        site = site,
                        task = type_of_task,
                        trial = j)]
            dat[, index := 1:.N, by = c('hand_sensor')]

            # fix time
            names(dat)[grep('time', tolower(names(dat)))] <- 'nulltime'
            dat[, 'nulltime' := NULL]
            dat[, time := index / 130.0]
            dat[, task := ifelse(any(str_detect(tolower(task), c('ptpun', 'u'))), 'unaff', 'aff')]
      
            # normalize session names (000/B to 000)
            dat[, day := ifelse(day == 'B', '000', day)]
            data_list[[j]] <- dat
        }
        data_list <- data_list[!sapply(data_list, is.null)]
        bigger_list[[i]] <- rbindlist(data_list)
        if (length(bigger_list[[i]]) == 0) { message(paste('No data:', sub_dirs[i])); next }
        
        # if the hand tends to have high variance, then it is the active one
        md_left_x <- abs(median(bigger_list[[i]][target_x > -90 & hand_sensor == 'left']$px))
        md_right_x <- abs(median(bigger_list[[i]][target_x > -90 & hand_sensor == 'right']$px))
        md_left_y <- abs(median(bigger_list[[i]][target_y > -90 & hand_sensor == 'left']$py))
        md_right_y <- abs(median(bigger_list[[i]][target_y > -90 & hand_sensor == 'right']$py))
        mean_left <- mean(md_left_x + md_left_y)
        mean_right <- mean(md_right_x + md_right_y)
        med_word <- mean_left < mean_right
        med_word <- ifelse(med_word, 'left', 'right')
        bigger_list[[i]][, engaged_hand := med_word]
        # only use relevant hand for data
        bigger_list[[i]] <- bigger_list[[i]][hand_sensor == engaged_hand]
    }
    bigger_list <- bigger_list[!sapply(bigger_list, is.null)]
    res <- rbindlist(bigger_list)
    
    # apply fixes--it seems that 552-003 L/R was mislabled?
    res[site == '552' & subject == '07' & day == '003', task := ifelse(task == 'aff', 'unaff', 'aff')]
    
    # normalize subject numbers
    res[,subject := as.character(as.integer(subject))]
    # convert site numbers to readable label (JHU, UZ, CU)
    res[, site := ifelse(site == '550', 'JHU', ifelse(site == '551', 'CU', 'UZ'))]
    
    # re-jigger target columns to make more sense
    res[, target_on := target_x > -90]
    res[, `:=`(target_x = target_x[1],
               target_y = target_y[1]),
        by = c('subject', 'day', 'site', 'task', 'trial')]
    # one more (now redundant) column
    res[, hand_sensor := NULL]
    res
}