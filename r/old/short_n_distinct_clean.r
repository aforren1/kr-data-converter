library(signal)

# Steps:
# Low-pass filter (butterworth, 8hz) & differenced for speed & acceleration
# peak speed at the first zero crossing of acceleration above a threshold of 10 cm/s.
# movement-start was defined as the time, before peak speed, when speed surpassed 2 cm/s
# The movement-end was defined as the time, after peak speed, when speed remained
#     less than 2 cm/s for more than 0.1 seconds
#
# Exclusion (label, but don't remove):
# direction at peak speed ≥90° away from target direction
# ending ≤30% of target distance
# What to do about movements that don't reach peak speed?

grp <- c('hand_sensor', 'subject', 'day', 'site', 'task', 'trial')

bf <- butter(5, 8/130, type = 'low') # TODO: check filter order (not listed in paper)

# only run things on the relevant hand
dat <- dat[engaged_hand == hand_sensor]

# re-jigger target columns to make more sense
dat[, target_on := target_x > -90]
dat[, `:=`(target_x = target_x[1],
           target_y = target_y[1]),
    by = grp]

# low pass filter
dat[, `:=`(sm_x = unclass(signal::filter(bf, hand_x)),
           sm_y = unclass(signal::filter(bf, hand_y))), 
    by = grp]
attr(dat$sm_x, 'tsp') <- NULL
attr(dat$sm_y, 'tsp') <- NULL

# extra helpers
dat[, angle_from_origin := atan2(sm_y, sm_x) * 180/pi]
dat[, angle_of_target := atan2(target_y, target_x) * 180/pi]
dat[, dist_away := sqrt((target_x-sm_x)^2 + (target_y-sm_y)^2)]
dat[, perc_of_dist := 1 - dist_away/sqrt(target_x^2 + target_y^2)]

# difference in angle between the hand and target (should be ~0 if hit target)
dat[, sm_pos := sqrt(sm_x ^ 2 + sm_y^2)]
dat[, sm_speed := abs(sm_pos - shift(sm_pos)) * 130, by = grp]
dat[, sm_accel := (sm_speed - shift(sm_speed)) * 130, by = grp]

# apply subsetting
dat[, move_sub := find_movement(sm_speed, sm_accel), by = grp]

# label good movements
dat[, good_move := label_good(subject, day, task, trial, 
                              sm_speed, angle_from_origin, 
                              angle_of_target, move_sub, 
                              perc_of_dist),
    by = grp]
# sanity check filter settings
tmpdat <- dat[subject==7 & site == 'UZ' & 
                day == '000' & task == 'aff' & trial == 18]

find_peak_speed <- function(speed) {
  peak_speed_index <- NA
  for (i in 1:length(speed)) {
    if (i == 1) { next }
    if (speed[i] > 0.1) {
      if (sign(accel[i]) != sign(accel[i-1])) {
        peak_speed_index <- i
        break
      }
    }
  }
  peak_speed_index
}

label_good <- function(subject, day, task, trial, 
                       speed, angle_from_origin, angle_of_target,
                       move_sub, perc_of_dist) {
  # What to do about movements that don't reach peak speed?
  peak_speed_index <- find_peak_speed(speed)
  good <- TRUE
  if (is.na(peak_speed_index)) {
    message('Peak speed not exceeded.')
    message(paste('Sub:', toString(subject[1]), 'day:', toString(day[1]),
                  'task:', toString(task[1]), 'trial:', toString(trial[1])))
    good <- FALSE
  }
  # direction at peak speed ≥90° away from target direction
  if (angle_from_origin[peak_speed_index] - angle_of_target[peak_speed_index] >= 90) {
    message('Direction at peak speed >= 90deg away from target')
    message(paste('Sub:', toString(subject[1]), 'day:', toString(day[1]),
                  'task:', toString(task[1]), 'trial:', toString(trial[1])))
    good <- FALSE
  }
  # ending ≤30% of target distance
  if (tail(perc_of_dist[move_sub], 1) <= 0.3) {
    message('Movement ended <= 30% of the target distance.')
    message(paste('Sub:', toString(subject[1]), 'day:', toString(day[1]),
                  'task:', toString(task[1]), 'trial:', toString(trial[1])))
    good <- FALSE  
  }
  return(rep(good, length(speed)))
}
find_movement <- function(speed, accel) {
  # step 1: iterate through the speeds > 10 cm/s, and 
  # find the first zero crossing in acceleration
  peak_speed_index <- find_peak_speed(speed)
  if (is.na(peak_speed_index)) {
    message('Peak speed not exceeded.')
    return(rep(FALSE, length(speed)))
  }
  
  # step 2: find the nearest index (in the past) where the speed exceeded 2cm/s
  # this is the start of the movement
  
  # first index ought to be the peak speed found earlier
  pre_speed <- rev(speed[1:peak_speed_index])
  for (i in 1:length(pre_speed)) {
    # find the first time the speed drops below/= 0.02, then get the prev index
    # (which should've been the first time *greater* than 0.02)
    if (pre_speed[i] <= 0.02) {
      beginning_index <- i - 1
      break
    }
  }
  beginning_index <- peak_speed_index - beginning_index + 1
  # step 3: find the nearest index (in future) where the speed drops below 2cm/s for 0.1s
  # this is the end of the movement
  post_speed <- speed[peak_speed_index+1:length(speed)]
  # it should spend 13 measurements below 2cm/s (given sampling rate of 130Hz)
  counter <- 0
  for (i in 1:length(post_speed)) {
    if (!is.na(post_speed[i]) & post_speed[i] <= 0.02) {
      counter <- counter + 1
    } else {
      counter <- 0
    }
    if (counter >= 13) {
      post_index <- i
      post_index <- post_index + peak_speed_index + 1
      break
    }
  }
  movement_subset <- rep(FALSE, length(speed))
  movement_subset[beginning_index:post_index] <- TRUE
  movement_subset
}
