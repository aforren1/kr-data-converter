
if (0) {
  # required packages
  install.packages(c('data.table', 'signal', 'ggplot2', 'ggforce', 'stringr', 'snakecase'))
}

library(ggplot2)
library(ggforce)

source('extract.r')
source('clean.r')

system.time(dat <- import_reach_data('C:/Users/aforren1/Downloads/A_SMARTS2SourceFiles/good'))
#system.time(dat <- import_reach_data('C:/Users/aforrence/Downloads/A_SMARTS2SourceFiles/good'))
#saveRDS(dat, 'raw_data.rds')
dat <- readRDS('raw_data.rds')
system.time(dat <- clean(dat))
# sanity check for counts per subject/day
# table(dat$subject, dat$day, dat$site)

# uncomment to save
# csv
# fwrite(dat, 'cleaned_data.csv')

# R-specific (much smaller)
# saveRDS(dat, 'cleaned_data.rds')