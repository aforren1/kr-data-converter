library(cowplot)

grp <- c('subject', 'day', 'site', 'task', 'trial')

subdat <- dat[subject == '2' &
                day == '090' &
                site == 'JHU' &
                engaged_hand == hand_sensor & task == 'aff' & trial > 24 & trial < 30]

pk_df <- unique(subdat[, pk_reached, by  = grp])

mark_start_end <- subdat[(use_for_subsetting)]
mark_start_end[, `:=`(min_idx = min(index),
                      max_idx = max(index)),
               by = grp]

mark_start_end <- unique(mark_start_end, by = grp)
mark_start_end <- melt(mark_start_end, id.vars = grp, measure.vars = c('min_idx', 'max_idx'))
# distance, speed, accel, jerk
p1 <- ggplot(subdat[index < 800],
       aes(x = index, y = sqrt(hand_x ^ 2 + hand_y ^2))) +
  geom_line(aes(y = sqrt(sm_x ^ 2 + sm_y ^2), colour = index)) +
  geom_line(aes(y = scales::rescale(sm_speed, to = c(0, 0.1))), colour = 'red') +
  geom_line(aes(y = scales::rescale(sm_acc, to = c(0, 0.1))), colour = 'green') +
  #geom_line(aes(y = scales::rescale(sm_jerk, to = c(0, 0.1))), colour = 'blue') +
  geom_vline(data = mark_start_end, aes(xintercept = value), linetype = 'longdash') +
  facet_wrap(~trial, nrow=1) +
  xlim(c(0, 800))

# position in x, y
p2 <- ggplot(subdat,
       aes(x = hand_x, y = hand_y, colour = index)) +
  geom_path(data = subdat[(use_for_subsetting)], 
            aes(x = hand_x, y = hand_y, colour = NULL), colour = 'red', size = 2) +
  geom_path(size = 1) +
  facet_wrap(~trial, nrow=1) +
  geom_text(data = pk_df, aes(x = 0, y = 0.025, label = as.character(pk_reached), colour = NULL))

plot_grid(p1, p2, ncol = 1)
