
Required packages can be found at the top of 'run_all.r'.

'cleaned_data.csv' and 'cleaned_data.rds' are the same (cleaned up) data, just different formats.

If running the cleaning, note that the path to the data is hard-coded (i.e. just sub out my 'C:/Users/...' with your own path)

Column explanations:

 - hand_x: x position of the hand
 - hand_y: y position of the hand
 - target_x: x position of the target; if ~-99, then target is not visible
 - target_y: y position of the target
 - hand_sensor: the originator of the hand_x & hand_y
 - subject: subject number (I think encoded as character?)
 - day: session #, ('000', '003', '090', '180')
 - site: ('UZ', 'JHU', 'CU')
 - task: 'aff'/'unaff' (affected side or unaffected)
 - trial: trial # (usually up to 80?)
 - index: index of the sample *within the trial* (and per sensor)
 - time: estimated time within the trial, in seconds
 - engaged_hand: hand currently being utilized by the patient
 - sm_x: smoothed x position of the hand (see 'clean.r')
 - sm_y: smoothed y *
 - rough_speed: speed from differencing of hand_x, hand_y
 - sm_speed: speed from the first derivative of the Savitzky-Golay filter coefficients
 - sm_acc: acceleration from the second *
 - pk_reached: whether the velocity of 0.1 m/s was reached in the trial

