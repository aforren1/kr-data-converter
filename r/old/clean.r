
library(signal)

# remove initial material from each trial

# for clipping movement,
# start when target appears (done already), 
# speed < 2 cm/s for more than 200ms *after* 
# the main speed peak (usually > 10cm/s)

grp <- c('hand_sensor', 'subject', 'day', 'site', 'task', 'trial')

calc_speed <- function(x, y) {
  sqrt((x - shift(x))^2 + (y - shift(y))^2)
}

segment_movement <- function(timestamps, vel, pk_threshold = 0.1, min_threshold = 0.02, end_dur = 0.1) {
  # pk_threshold is the minimal speed attained to count toward a peak
  # min_threshold is the amount that must be exceeded at the beginning of a movement,
  # and must be not exceeded for 100ms at the end of a movement
  # from Juan's paper (Short n Distinct, 2017)
  total_len <- length(vel)
  res <- rep(FALSE, total_len)
  start_time <- NULL
  
  # filter out early suprathreshold vals (i.e. already moving when target comes on)
  initial_offset <- min(which(vel < min_threshold))
  
  # find first val greater than pk_threshold
  pk_index <- min(which(vel[initial_offset:total_len] >= pk_threshold)) + (initial_offset - 1)
  
  # find the first index of vel > threshold (searching from beginning)
  initial_index <- min(which(vel[initial_offset:total_len] >= min_threshold)) + (initial_offset - 1)
  if (!is.finite(initial_index)) { initial_index <- 1}

  if (is.finite(pk_index)) {
    pk_reached <- TRUE

    # search forward from pk_index, finding the first end_dur sec where vel < min_threshold
    for (i in 1:length(vel)) {
      if (i < pk_index) { next } # ignore first chunk
      if (vel[i] < min_threshold) {
        if (is.null(start_time)) {
          start_time <- timestamps[i]
        }
        delta <- timestamps[i] - start_time
        if (delta >= end_dur) {
          break # current index i is the end of movement
        }
      }
      else if (vel[i] >= min_threshold) {
        start_time <- NULL
      }
      
    }
  } else { # do different things if no movements exceed threshold
    pk_reached <- FALSE
    for (i in 1:length(vel)) {
      # we search from the initial_index, rather than pk_index
      if (i < initial_index) { next }
      if (vel[i] < min_threshold) {
        if (is.null(start_time)) {
          start_time <- timestamps[i]
        }
        delta <- timestamps[i] - start_time
        if (delta >= end_dur) {
          break # current index i is the end of movement
        }
      }
      else if (vel[i] >= min_threshold) {
        start_time <- NULL
      }
    }
  }
  # initial_index:i should be start:end of movement
  res[initial_index:i] = TRUE
  
  return(list(res, pk_reached)) # use res for subsetting, pk_reached indicates whether the pk_threshold was reached
}

clean <- function(dat) {
  # these are empirically derived
  # likely oversmoothed, but the point is to get on/offset
  p <- 3 # filter order
  n <- 51 # filter length
  sg_0 <- sgolay(p = p, n = n, m = 0) # pos
  sg_1 <- sgolay(p = p, n = n, m = 1) # speed
  sg_2 <- sgolay(p = p, n = n, m = 2) # accel
  sg_3 <- sgolay(p = p, n = n, m = 3) # jerk
  system.time(dat[, `:=`(sm_x = signal::sgolayfilt(hand_x, sg_0),
             sm_y = sgolayfilt(hand_y, sg_0),
             sm_dx = sgolayfilt(hand_x, sg_1), # est. speed
             sm_dy = sgolayfilt(hand_y, sg_1),
             sm_ddx = sgolayfilt(hand_x, sg_2), # est. accel
             sm_ddy = sgolayfilt(hand_y, sg_2),
             sm_dddx = sgolayfilt(hand_x, sg_3),# est. jerk
             sm_dddy = sgolayfilt(hand_y, sg_3)),
      by = grp])
  dat[, rough_speed := calc_speed(hand_x, hand_y) * 130, by = grp] # for reference
  dat[, sm_speed := sqrt(sm_dx^2 + sm_dy^2) * 130, by = grp] # what we'll use for segmenting movement
  dat[, sm_acc := sqrt(sm_ddx^2 + sm_ddy^2) * 130, by = grp]
  dat[, sm_jerk := sqrt(sm_dddx^2 + sm_dddy^2) * 130, by = grp]
  dat[, c('sm_dx', 'sm_dy', 'sm_ddx', 'sm_ddy', 'sm_dddx', 'sm_dddy') := NULL]
  
  dat[hand_sensor==engaged_hand, c('use_for_subsetting', 'pk_reached') := segment_movement(time, sm_speed), by = grp]
  dat <- dat[hand_sensor==engaged_hand]
  #dat <- dat[(use_for_subsetting)]
  #dat[, use_for_subsetting := NULL]
  #dat
  # filter out reaches that never exceed starting circle
  # 0.01 is the radius of the starting circle (I think)
  # dat[, mag := sqrt(sm_x^2 + sm_y^2)]
  # dat[, mag_not_exceed := !any(mag >= 0.01), by = grp]
  # dat <- dat[(!mag_not_exceed)]
}
# idea of how many sub-threshold reaches per person, side, etc.
# tmp1 <- dat[(!pk_reached), TRUE, by = grp]
# tmp1[, length(N), by = c('hand_sensor', 'subject', 'day', 'site', 'task')]

# UZ-05 day 000 is always subthreshold
# ggplot(dat[subject == '7' & site == 'UZ' &
#              day == '000' & task == 'aff' & time < 20 & trial < 40],
#        aes(x = time, y = sm_speed)) +
#   facet_wrap(~trial) +
#   geom_hline(yintercept=0.02,linetype='longdash') +
#   geom_line(aes(y = rough_speed), colour = 'red') +
#   geom_line()

# # get an idea of how pos, vel, acc change in a patient
# ggplot(dat[hand_sensor==engaged_hand & subject == '7' & site == 'UZ' & 
#              day == '000' & task == 'aff' & time < 30 & trial < 40],
#        aes(x = time, y = sm_speed)) +
#   geom_line(aes(y = sm_acc*20), colour = 'red') +
#   facet_wrap(~trial) +
#   geom_line() + geom_line(aes(y = sm_x), colour = 'blue') +
#   geom_line(aes(y = hand_x), colour = 'blue', linetype = 'longdash')
# 
# 
# # sanity:
# ggplot(dat[subject == '7' & site == 'UZ' & task == 'unaff'],
#        aes(x = sm_x, y = sm_y, group = trial)) +
#   geom_path(alpha=0.4, colour = 'red', aes(x = hand_x, y = hand_y)) +
#   geom_path(alpha=0.4) +
# ggforce::geom_circle(data = data.frame(x = 0, y = 0, r = 0.01), # start is 0.01, target is 0.015
#                      aes(x = NULL, y = NULL, group=NULL, x0 = x, y0 = y, r = r), size = 1.5) +
# ggforce::geom_circle(data = unique(dat[target_x > -90][,c('target_x', 'target_y')]),
#                      aes(x = NULL, y = NULL, group = NULL,
#                          x0 = target_x, y0 = target_y, r = 0.015),
#                      size = 1.5, colour = 'green') + facet_wrap(~day)

## check how much is chopped off (need to call before dat <- dat[(use_for_subsetting)])
# 
# ggplot(dat[subject == '11' & site == 'UZ' & day == '003' & task == 'aff' & trial < 30],
#        aes(x = sm_x, y = sm_y)) +
#   ggforce::geom_circle(data = data.frame(x = 0, y = 0, r = 0.01), # start is 0.01, target is 0.015
#                        aes(x = NULL, y = NULL, group=NULL, x0 = x, y0 = y, r = r), size = 1.5, alpha=0.5) +
#   ggforce::geom_circle(data = unique(dat[target_x > -90][,c('target_x', 'target_y')]),
#                        aes(x = NULL, y = NULL, group = NULL,
#                            x0 = target_x, y0 = target_y, r = 0.015),
#                        size = 1.5, colour = 'green') +
#   facet_wrap(~trial) + geom_path(size=1.1) +
#   geom_path(data = dat[subject == '11' & site == 'UZ' & day == '003' & task == 'aff' & use_for_subsetting & trial < 30], 
#             aes(x = sm_x, y = sm_y), colour = 'red', size=1.5, alpha=0.5)