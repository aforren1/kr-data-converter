library(data.table)
library(stringr)
library(snakecase)

# notes:
# UZ uses 000 instead of B
# UZ uses e.g. 552-04_000_U instead of PtpUn
# UZ timestamps are in seconds, and are useless early on
# (missing the decimal component, which was added later)
# check 552-07
# check active hand heuristics
# 

# from https://stackoverflow.com/questions/29214932/split-a-file-path-into-folder-names-vector
split_path <- function(path, mustWork = FALSE, rev = TRUE) {
  output <- c(strsplit(dirname(normalizePath(path,mustWork = mustWork)),
                       "/|\\\\")[[1]], basename(path))
  ifelse(rev, return(rev(output)), return(output))
}

is_error <- function(x) inherits(x, 'try-error')
# find all subdirectories w/ a tfile
find_tfiles <- function(x) { length(list.files(x, pattern = '.tgt')) == 1 }
# find the point-to-point blocks
find_ptp <- function(x) { !grepl('meas', tolower(basename(x))) }

import_reach_data <- function(base_dir) {
  dirs <- list.dirs(base_dir, full.names = TRUE, recursive = TRUE)
  
  sub_dirs <- dirs[sapply(dirs, find_tfiles)]
  
  sub_dirs <- sub_dirs[sapply(sub_dirs, find_ptp)]
  
  bigger_list <- list()
  for (i in 1:length(sub_dirs)) {
    # check if directory is worthy
    all_data_files <- list.files(sub_dirs[i], pattern='.dat', full.names=TRUE)
    all_data_files <- all_data_files[grepl('Trial', all_data_files)]
    if (length(all_data_files) < 2) { message(paste('Short block, skipping:', sub_dirs[i])); next }
    numbs <- sapply(basename(all_data_files), substr, 7, 9, USE.NAMES = FALSE)
    if (anyDuplicated(numbs)) { message(paste('Duplicated trial numbers, something wrong with:', sub_dirs[i])); next }
    # check if tfile is worthy (skip if weird, who knows where the data came from)
    tfile <- try(fread(file.path(sub_dirs[i], 'tFile.tgt'), header = FALSE))
    if (is_error(tfile)) { message(paste('empty tfile:', sub_dirs[i])); next }
    if (nrow(tfile) < 2) { message(paste('short tfile:', sub_dirs[i])); next }
    names(tfile) <- c('dur', 'start_x', 'start_y', 'target_x', 'target_y', 'unused')
    # figure out experiment stuff
    path_stuff <- split_path(sub_dirs[i])
    message(path_stuff[1])
    # e.g. ptpAF, ptpUN, U, A... (UZ includes # & such, so gsub)
    tmp <- gsub('[0-9]', '', x = path_stuff[1])
    type_of_task <- gsub('[[:punct:]]', '', x = tmp)
    site <- substr(path_stuff[2], 1, 3) # first three numbers are the site
    
    # next 1 to 3 numbers are the subject
    # strong assumption that underscores aren't used early...
    first_us <- str_locate(path_stuff[2], '_')[1] - 1
    first_chunk <- substr(path_stuff[2], 4, first_us)
    subject_id <- gsub('[^0-9]', '', first_chunk)
    day <- strsplit(path_stuff[2], '_')[[1]][2]
    
    data_list <- list()
    for (j in 1:length(all_data_files)) {
      # read trial-by-trial
      # check if the data is worthy
      # sanity check lengths
      header <- try(fread(all_data_files[j], skip = 7, nrows = 0, drop = c(11, 12)))
      if (is_error(header)) { message(paste('empty header:', all_data_files[j])); next }
      # These were the couple of sessions at UZ with a bad timestamp + inconsistent sampling
      if ('fake_time' %in% names(header) && site=='552') { next }
      
      dat <- try(fread(all_data_files[j], skip = 9, header = FALSE, drop = c(11, 12)))
      if (is_error(dat)) { message(paste('empty data:', all_data_files[j])); next }
      if (nrow(dat) < 10) { message(paste('short data:', all_data_files[j])); next }
      
      # add header
      names(dat) <- to_snake_case(names(header))
      # check whether there are any in-trial data
      if (!any(dat$target_x > -90)) { message(paste('no in-trial data:', all_data_files[j])); next}
      dat <- dat[min(which(target_x > -90)):.N] # remove pre-trial mess
      dat[, `:=`(hand_x = hand_x - start_x,
                 hand_y = hand_y - start_y,
                 target_x = target_x - start_x,
                 target_y = target_y - start_y)]
      dat[, c('start_x', 'start_y', 'velocity') := NULL] # velocity was calc'd online
      dat <- dat[device_num == 1 | device_num == 3]
      dat[, hand_sensor := ifelse(device_num == 1, 'left', 'right')]
      dat[, device_num := NULL]
      dat[, hand_z := NULL]

      dat[, `:=`(subject = subject_id,
                 day = day,
                 site = site,
                 task = type_of_task,
                 trial = j)]
      dat[, index := 1:.N, by = c('hand_sensor')]
      # sanity check:
      # ggplot(dat, aes(x = hand_x, y = hand_y)) + 
      #   geom_path() + facet_wrap(~hand_sensor) + 
      #   coord_fixed(xlim = c(-0.1, 0.1), ylim = c(-0.1, 0.1)) + 
      #   geom_point(data = dat[1], aes(x = target_x, y = target_y), 
      #              size = 5, shape = 21, colour = 'red')
      
      # normalize time (looking at you, UZ)
      # Note that UZ 'fake_time's are garbage, it looks like for the first part
      # the timestamp is missing the ms part (in 's.ms' format)
      # Then they switch to 'TrackerTime', which does include the ms component
      # This currently isn't 
      if ('fake_time' %in% names(dat)) { # Hopkins, Columbia, and part of UZ use this
        dat[, fake_time := fake_time / 1000.0] # change to seconds
      }
      if ('fake_time' %in% names(dat) & dat$site[1] == '552') {
        message(paste('Suspect time:', all_data_files[j]))
      }
      names(dat)[grep('time', tolower(names(dat)))] <- 'nulltime'
      dat[, 'nulltime' := NULL]
      dat[, time := index / 130.0]
      
      # normalize task names (ptpun/ptpaf & u/a to unaff/aff)
      dat[, task := ifelse(any(str_detect(tolower(task), c('ptpun', 'u'))), 'unaff', 'aff')]
      
      # normalize session names (000/B to 000)
      dat[, day := ifelse(day == 'B', '000', day)]
      
      # grow the data.table list
      data_list[[j]] <- dat
    }
    # sanity check:
    # ggplot(data_list[hand_sensor=='left' & target_x > -90], aes(x = hand_x, y = hand_y, group = trial)) + 
    #   geom_path(alpha=0.3) + 
    #   geom_point(shape = 21, size = 5, colour = 'red', data = data_list[, .SD[1], trial], 
    #              aes(x = target_x, y = target_y))
    data_list <- data_list[!sapply(data_list, is.null)]
    bigger_list[[i]] <- rbindlist(data_list)
    if (length(bigger_list[[i]]) == 0) { message(paste('No data:', sub_dirs[i])); next }
    
    # if the hand tends to have high variance, then it is the active one
    md_left_x <- abs(median(bigger_list[[i]][target_x > -90 & hand_sensor == 'left']$hand_x))
    md_right_x <- abs(median(bigger_list[[i]][target_x > -90 & hand_sensor == 'right']$hand_x))
    md_left_y <- abs(median(bigger_list[[i]][target_y > -90 & hand_sensor == 'left']$hand_y))
    md_right_y <- abs(median(bigger_list[[i]][target_y > -90 & hand_sensor == 'right']$hand_y))
    mean_left <- mean(md_left_x + md_left_y)
    mean_right <- mean(md_right_x + md_right_y)
    med_word <- mean_left < mean_right
    med_word <- ifelse(med_word, 'left', 'right')
    message(paste('median says ', med_word))
    message(paste('left:', str(mean_left), 'right:', str(mean_right)))
    message('----')
    
    bigger_list[[i]][, engaged_hand := med_word]
  }
  bigger_list <- bigger_list[!sapply(bigger_list, is.null)]
  res <- rbindlist(bigger_list)
  
  # apply fixes--it seems that 552-003 L/R was mislabled?
  res[site == '552' & subject == '07' & day == '003', task := ifelse(task == 'aff', 'unaff', 'aff')]
  
  # normalize subject numbers
  res[,subject := as.character(as.integer(subject))]
  # convert site numbers to readable label (JHU, UZ, CU)
  res[, site := ifelse(site == '550', 'JHU', ifelse(site == '551', 'CU', 'UZ'))]
  res
}
# sanity check (using just UZ data):
# dat <- import_reach_data('C:/Users/aforrence/Downloads/UZ')
# dat[, day := factor(day, levels = c('000', '003', '090', '180'))]
# ggplot(dat[target_x > -90 & engaged_hand == hand_sensor & subject %in% c('03', '04', '05')],
#        aes(x = hand_x, y = hand_y, group = trial)) +
#   geom_path(alpha=0.3) +
#   facet_grid(task + hand_sensor~day)

# e.g. count number of obs per subject, hand, day: xx[, .N, by = c('subject', 'engaged_hand', 'day')][1:30]
# e.g. count number of aff/unaff, see if it matches # of calculated "engaged_hand"
# table(unique(xx, by = c('subject', 'engaged_hand', 'day'))$engaged_hand)
# table(unique(xx, by = c('subject', 'engaged_hand', 'day'))$task)

# look for consistency in rating: xx[,.N,by = c('subject', 'day', 'engaged_hand', 'task')][1:50]
# esp. 07

# for clipping movement,
# start when target appears, velocity < 2 cm/s for more than 200ms *after* 
# the main velocity peak (usually > 12cm/s)
