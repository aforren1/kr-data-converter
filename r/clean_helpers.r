find_peak_speed <- function(speed, accel) {
  peak_speed_index <- NA
  for (i in 1:length(speed)) {
    if (i < 5) { next }
    if (speed[i] > 0.1) {
      if (sign(accel[i]) != sign(accel[i-1])) {
        peak_speed_index <- i
        break
      }
    }
  }
  peak_speed_index
}

label_good <- function(subject, day, site, task, trial, 
                       speed, accel, angle_from_origin, angle_of_target,
                       move_sub, perc_of_dist) {
  # What to do about movements that don't reach peak speed?
  peak_speed_index <- find_peak_speed(speed, accel)
  good <- TRUE
  if (is.na(peak_speed_index)) {
    message('Peak speed not exceeded.')
    message(paste('Sub:', toString(subject[1]), 'day:', toString(day[1]),
                  'task:', toString(task[1]), 'trial:', toString(trial[1])))
    good <- FALSE
    return(rep(good, length(speed)))
  }
  if (!any(move_sub)) {
    message('No peak or no ending?')
    message(paste('Sub:', toString(subject[1]), 'day:', toString(day[1]),
                  'task:', toString(task[1]), 'trial:', toString(trial[1])))
    good <- FALSE
    return(rep(good, length(speed)))
  }
  # direction at peak speed ≥90° away from target direction
  if (angle_from_origin[peak_speed_index] - angle_of_target[peak_speed_index] >= 90) {
    message('Direction at peak speed >= 90deg away from target')
    message(paste('Sub:', toString(subject[1]), 'day:', toString(day[1]),
                  'task:', toString(task[1]), 'trial:', toString(trial[1])))
    good <- FALSE
    return(rep(good, length(speed)))
  }
  # ending ≤30% of target distance
  if (tail(perc_of_dist[move_sub], 1) <= 0.3) {
    message('Movement ended <= 30% of the target distance.')
    message(paste('Sub:', toString(subject[1]), 'day:', toString(day[1]),
                  'task:', toString(task[1]), 'trial:', toString(trial[1])))
    good <- FALSE  
    return(rep(good, length(speed)))
  }
  return(rep(good, length(speed)))
}
find_movement <- function(speed, accel) {
  # step 1: iterate through the speeds > 10 cm/s, and 
  # find the first zero crossing in acceleration
  peak_speed_index <- find_peak_speed(speed, accel)
  if (is.na(peak_speed_index)) {
    message('Peak speed not exceeded.')
    return(rep(FALSE, length(speed)))
  }
  
  # step 2: find the nearest index (in the past) where the speed exceeded 2cm/s
  # this is the start of the movement
  
  # first index ought to be the peak speed found earlier
  pre_speed <- rev(speed[1:peak_speed_index])
  for (i in 1:length(pre_speed)) {
    # find the first time the speed drops below/= 0.02, then get the prev index
    # (which should've been the first time *greater* than 0.02)
    if (pre_speed[i] <= 0.02) {
      beginning_index <- i - 1
      break
    }
  }
  beginning_index <- peak_speed_index - beginning_index + 1
  # step 3: find the nearest index (in future) where the speed drops below 2cm/s for 0.1s
  # this is the end of the movement
  post_speed <- speed[peak_speed_index+1:length(speed)]
  # it should spend 13 measurements below 2cm/s (given sampling rate of 130Hz)
  post_index <- NA
  counter <- 0
  for (i in 1:length(post_speed)) {
    if (!is.na(post_speed[i]) & post_speed[i] <= 0.02) {
      counter <- counter + 1
    } else {
      counter <- 0
    }
    if (counter >= 13) {
      post_index <- i
      post_index <- post_index + peak_speed_index + 1
      break
    }
  }
  if (is.na(post_index)) { 
    message(paste('Ending not detected. Counter val:', counter))
    return(rep(FALSE, length(speed)))
  }
  movement_subset <- rep(FALSE, length(speed))
  movement_subset[beginning_index:post_index] <- TRUE
  movement_subset
}
