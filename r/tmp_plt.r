library(plotly)

tmp <- dat[subject == '552-7' & day=='180' & task=='aff']

accumulate_by <- function(dat, var) {
  # builds up a *huge* dataframe with the accumulation of all prev frames
  var <- lazyeval::f_eval(var, dat)
  lvls <- plotly:::getLevels(var)
  dats <- lapply(seq_along(lvls), function(x) {
    cbind(dat[var %in% lvls[seq(1, x)], ], frame = lvls[[x]])
  })
  dplyr::bind_rows(dats)
}

xx <- tmp[trial==10] %>% accumulate_by(~index)

p1 <- xx %>% plot_ly(x = ~sm_x, y = ~sm_y, split = ~trial, frame=~frame, 
               type='scatter', mode='lines', line=list(simplify=FALSE))%>% 
  layout(xaxis=list(range=c(-0.1, 0.1)), yaxis=list(range=c(-0.1, 0.1), scaleanchor='x'),
         shapes=list(
           list(type='circle', xref='x', x0=~target_x[1] - 0.005, x1=~target_x[1] + 0.005,
                yref='y', y0=~target_y[1] + 0.005, y1=~target_y[1] - 0.005,
                fillColor='rgb(50, 20, 90)', opacity=0.2)
         )) %>% 
  animation_opts(
    frame = (1/130)*1000, # play at real speed
    transition = 0, 
    redraw = FALSE
  )

p2 <- tmp[trial==10] %>% plot_ly(x = ~index, y = ~sm_speed, split=~trial,
                     type='scatter', mode='lines', line=list(simplify=FALSE))

p3 <- tmp[trial==10] %>% plot_ly(x = ~index, y = ~sm_accel, split=~trial,
                                 type='scatter', mode='lines', line=list(simplify=FALSE))

subplot(p2, p3, shareX = TRUE, nrows=2)

