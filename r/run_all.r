library(signal)
library(ggplot2)

source('import.r')
source('tmp.r')
grp <- c('subject', 'day', 'site', 'task', 'trial')

system.time(
    dat <- import_reach_data('~/extra/A_SMARTS2SourceFiles/good')
    #dat <- import_reach_data('C:/Users/aforrence/Downloads/A_SMARTS2SourceFiles/good')
    #dat <- import_reach_data('C:/Users/aforren1/Downloads/A_SMARTS2SourceFiles/good')
)
# fix subject name
dat[, subject := paste0(ifelse(site == 'CU', '551-', ifelse(site == 'UZ', '552-', '550-')), subject)]
# saveRDS(dat, 'tmp.rds')
# cleaning material

# Steps:
# Low-pass filter (butterworth, 8hz) & differenced for speed & acceleration
# peak speed at the first zero crossing of acceleration above a threshold of 10 cm/s.
# movement-start was defined as the time, before peak speed, when speed surpassed 2 cm/s
# The movement-end was defined as the time, after peak speed, when speed remained
#     less than 2 cm/s for more than 0.1 seconds
#
# Exclusion (label, but don't remove):
# direction at peak speed ≥90° away from target direction
# ending ≤30% of target distance (regardless of target?)
# What to do about movements that don't reach peak speed?
fs <- 130
if (TRUE) {
  #bf <- butter(3, 8/(fs/2), type = 'low')
  bf <- butter(3, 8/(fs/2), type = 'low')
  
  # low pass filter
  dat[, `:=`(sm_x = unclass(signal::filtfilt(bf, px)),
             sm_y = unclass(signal::filtfilt(bf, py))),
      by = grp]
  attr(dat$sm_x, 'tsp') <- NULL
  attr(dat$sm_y, 'tsp') <- NULL
  
  dat[, sm_speed := sqrt((sm_x - shift(sm_x))^2 + (sm_y - shift(sm_y))^2) / (time - shift(time)), by = grp]
  dat[, sm_accel := sm_speed - shift(sm_speed), by = grp]

} else {
  p <- 3
  n <- 51
  sg_0 <- sgolay(p = p, n = n, m = 0) # pos
  dat[, `:=`(sm_x = sgolayfilt(px, sg_0),
             sm_y = sgolayfilt(py, sg_0)),
      by = grp]
  dat[, sm_pos := sqrt(sm_x ^ 2 + sm_y^2)]
  dat[, sm_speed := abs(sm_pos - shift(sm_pos)) * fs, by = grp]
  dat[, sm_accel := (sm_speed - shift(sm_speed)) * fs, by = grp]
}
# extra helpers
dat[, angle_from_origin := atan2(sm_y, sm_x) * 180/pi]
dat[, angle_of_target := atan2(target_y, target_x) * 180/pi]
dat[, dist_away := sqrt(sm_x^2 + sm_y^2)] # apparently it's just the *magnitude*
dat[, perc_of_dist := dist_away/sqrt(target_x^2 + target_y^2)]

# label subsets of movements, and various properties
# use .BY to access group info (removing for now...)
dat[, c('movement_subset', 'peak_exceeded', 'direction_good', 'distance_good') :=
      label_movement(
        sm_speed,
        sm_accel,
        angle_from_origin,
        angle_of_target,
        perc_of_dist
      ),
    by = grp]

dat[, right_dir := label_turn_point(sm_x, sm_y, sm_speed, sm_accel, 
                                    angle_of_target[1], target_x[1], target_y[1],
                                    peak_exceeded), by = grp]


#dat <- dat[(right_dir & movement_subset & peak_exceeded & 
#              direction_good & distance_good)]
# check the displacement in the final subset, see if we made it at least 1/2 the distance
# can only check this after we've done all other subsetting
dat[(right_dir & movement_subset & peak_exceeded & 
       direction_good & distance_good), 
    good_displ := at_least_half(sm_x, sm_y, target_x[1], target_y[1]), by = grp]
# check that the reach starts within a third of the distance to the origin
dat[(right_dir & movement_subset & peak_exceeded & 
       direction_good & distance_good & good_displ), 
    close_start := close_to_start(sm_x, sm_y, target_x[1], target_y[1]), by = grp]

# saveRDS(dat, file = 'cleaned-data.rds')
subdat <- dat[subject == '552-5' & day == '000' & site == 'UZ' & task == 'aff' & trial > 21 & trial < 50]
# subject == 7 & day == '003' & site == 'UZ' & task == 'aff' (misbehaved)
# subject == 10 & day == '000' & site == 'UZ' & task == 'aff' (misbehaved)

labdat <- subdat[, .SD[1], by = grp]
ggplot(subdat, aes(x = sm_x, y = sm_y))  + 
  geom_label(data = labdat, aes(x = -Inf, y = -Inf, label = paste0('Distance good:', distance_good,
                                                                   '\nDirection good:', direction_good,
                                                                   '\nPeak exceeded:', peak_exceeded)), 
             hjust=-0.1, vjust=-0.1) +
  geom_point(data = labdat, aes(x = target_x, y = target_y), size=5, shape=21, colour = 'green', stroke = 2) +
  geom_path(size=1) + 
  geom_path(data = subdat[(movement_subset)], colour = 'red', size=1) + 
  facet_wrap(~trial) +
  xlim(c(-0.1, 0.1)) +
  ylim(c(-0.1, 0.1)) +
  coord_fixed()

subdat <- dat[subject == '552-7' & day == '090' & task == 'unaff' & trial <6]