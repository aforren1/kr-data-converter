library(ggplot2)

dat2 <- dat[(right_dir & movement_subset & peak_exceeded & 
             direction_good & distance_good & good_displ & close_start)]
labdat <- dat[subject == '550-2' & day == '180' & task == 'aff', .SD[1], by = grp]
p1 <- ggplot(dat[subject == '550-2' & day == '180' & task == 'aff'], 
       aes(x = sm_x, y = sm_y, colour = angle_of_target, group = trial)) +
  geom_path() +
  facet_wrap(~trial) + geom_path(data = dat2[subject == '550-2' & day == '180' & task == 'aff'], 
                                 aes(x = sm_x, y = sm_y), colour = 'red') +
  geom_label(data = labdat, aes(x = -Inf, y = -Inf, label = paste0('right_dir:', right_dir,
                                                                   '\nDirection good:', direction_good,
                                                                   '\nPeak exceeded:', peak_exceeded)), 
             hjust=-0.1, vjust=-0.1, size = 2) 

p2 <- ggplot(dat[subject == '550-2' & day == '180' & task == 'aff'], aes(x = time, y = sm_speed)) + 
  geom_line() + facet_wrap(~trial) + 
  geom_line(data = dat2[subject == '550-2' & day == '180' & task == 'aff'],
            aes(x = time, y = sm_speed), colour = 'red')

grid.arrange(p1, p2, ncol=2)

p1 <- ggplot(dat[subject == '552-21' & day == '000' & task == 'aff'], 
             aes(x = sm_x, y = sm_y, colour = angle_of_target, group = trial)) +
  geom_path() +
  facet_wrap(~trial) + geom_path(data = dat2[subject == '552-21' & day == '000' & task == 'aff'], 
                                 aes(x = sm_x, y = sm_y), colour = 'red')

p2 <- ggplot(dat[subject == '552-21' & day == '000' & task == 'aff'], aes(x = time, y = sm_speed)) + 
  geom_line() + facet_wrap(~trial) + 
  geom_line(data = dat2[subject == '552-21' & day == '000' & task == 'aff'],
            aes(x = time, y = sm_speed), colour = 'red')

grid.arrange(p1, p2, ncol=2)

# look at right_dir (which looks at direction in trials where peak reached)
# and movement_subset (which looks at velocity profile)
dat2 <- dat[subject == '550-2' & day == '180' & task == 'aff']
labdat <- dat2[, .SD[1], by = grp]
p1 <- ggplot(dat2, 
             aes(x = sm_x, y = sm_y, colour = angle_of_target, group = trial)) +
  geom_path() +
  facet_wrap(~trial) + geom_path(data = dat2[(right_dir)], 
                                 aes(x = sm_x, y = sm_y), colour = 'red') +
  geom_path(data = dat2[(movement_subset)], 
            aes(x = sm_x, y = sm_y), colour = 'blue') 


p2 <- ggplot(dat2, aes(x = time, y = sm_speed)) + 
  geom_line() + facet_wrap(~trial) + 
  geom_line(data = dat2[(right_dir)],
            aes(x = time, y = sm_speed), colour = 'red') + 
  geom_line(data = dat2[(movement_subset)], 
            aes(x = time, y = sm_speed), colour = 'blue')

grid.arrange(p1, p2, ncol=2)